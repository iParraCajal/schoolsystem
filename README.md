# SchoolSystem

La indea es hacer un sistema para poder administrar una escuela

- Django rest framework: https://www.django-rest-framework.org/tutorial/quickstart/
- Docker config: https://github.com/docker/awesome-compose/tree/master/official-documentation-samples/django/
- Swagger setup: https://github.com/axnsan12/drf-yasg/#installation

## Setup

### Create virtualenv

```
python3 -m venv <name>
```

or

```
virtualenv <name>
```

### Activate virtualenv

on linux

```
source <name>/bin/activate
```

on windows

```
source <name>/Scripts/activate
```

### Install dependencies

```
pip install -r requirements.txt
```

## Run app

```
python manage.py runserver 0.0.0.0:8000
```

once the server is up and running you can go to:

- django admin: http://localhost:8000/admin
- swagger api doc: http://localhost:8000/swagger

## Docker

### Build app

```
docker compose build
```

### Run app

```
docker compose up
```
