from django.apps import AppConfig


class SystemSchoolConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'system_school'
